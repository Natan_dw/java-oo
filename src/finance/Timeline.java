package finance;

public class Timeline {
    public String name;
    public int year;
    public float January;
    public float February;
    public float March;
    public float April;
    public float May;
    public float June;
    public float July;
    public float August;
    public float September;
    public float October;
    public float November;
    public float December;

    public void setValueMonthly(){
        System.out.println("TimeLine year:" + year);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public float getJanuary() {
        return January;
    }

    public void setJanuary(float january) {
        January = january;
    }

    public float getFebruary() {
        return February;
    }

    public void setFebruary(float february) {
        February = february;
    }

    public float getMarch() {
        return March;
    }

    public void setMarch(float march) {
        March = march;
    }

    public float getApril() {
        return April;
    }

    public void setApril(float april) {
        April = april;
    }

    public float getMay() {
        return May;
    }

    public void setMay(float may) {
        May = may;
    }

    public float getJune() {
        return June;
    }

    public void setJune(float june) {
        June = june;
    }

    public float getJuly() {
        return July;
    }

    public void setJuly(float july) {
        July = july;
    }

    public float getAugust() {
        return August;
    }

    public void setAugust(float august) {
        August = august;
    }

    public float getSeptember() {
        return September;
    }

    public void setSeptember(float september) {
        September = september;
    }

    public float getOctober() {
        return October;
    }

    public void setOctober(float october) {
        October = october;
    }

    public float getNovember() {
        return November;
    }

    public void setNovember(float november) {
        November = november;
    }

    public float getDecember() {
        return December;
    }

    public void setDecember(float december) {
        December = december;
    }

    
}
